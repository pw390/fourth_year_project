import numpy as np
import pickle
import models as md
import plot as pl
import matplotlib.pyplot as pt

file_name = 'SNP_2.dat'
with open(file_name,'rb') as f:
    y_t= pickle.load(f)

N = 1000
# start time
start_time = 700
# end time
end_time = 800
# time spread
Duration = end_time - start_time + 1
# constant tick size
tick_size = 1e-5
# mini size
mini_size = 5e4
#get absolute price difference
for i in y_t:
    i.miniportioned(mini_size)

sigma_alpha = 0.05
sigma_beta = 0.5e-5
sigma_mid_price= 0.5e-5

model = md.GammaFlexMidpriceModel(sigma_alpha, sigma_beta, sigma_mid_price)

parameter = np.vstack((np.ones([1,N])*1.2,
                       np.ones([1,N])*5e-5,
                       np.ones([1,N])*1.2,
                       np.ones([1,N])*5e-5,
                       np.ones([1,N])*y_t[start_time].mid_price))

ave_parameter = np.zeros([5,Duration])
std_parameter = np.zeros([5,Duration])

for i in range(start_time, end_time+1):
    print(i)
    parameter = model.sample(parameter)
    weight = model.log_likelihood(y_t[i], parameter, tick_size)
    weight = model.weight_normalisation(weight)
    parameter = model.stratified_sampling(weight,parameter)
    ave_parameter[:,i-start_time] = np.mean(parameter,axis = 1)
    std_parameter[:,i-start_time] = np.std(parameter, axis=1)

pl.parameter_curve(ave_parameter,std_parameter,['bid_alpha','bid_beta',
                                                'ask_alpha', 'ask_beta',
                                                 'mid_price'])


y_mid_price = np.zeros([1,Duration])

for i in range(start_time, end_time+1):
    y_mid_price[0,i-start_time] = y_t[i].mid_price
print (y_mid_price)
pt.plot(np.transpose(y_mid_price))
pt.show()