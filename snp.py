class snp(object):
    def __init__(self):
        self.bid = {}
        self.ask = {}

    def insert(self, price, volume, ask_bid):
        if ask_bid == 1:
            if price in self.bid:
                self.bid[price] += volume
            else:
                self.bid[price] = volume
        else:
            if price in self.ask:
                self.ask[price] += volume
            else:
                self.ask[price] = volume

    def cancel_order(self, price, volume, ask_bid):
        if ask_bid == 1:
            self.bid[price] -= volume
            if self.bid[price] == 0:
                del self.bid[price]
        else:
            self.ask[price] -= volume
            if self.ask[price] == 0:
                del self.ask[price]

    def new_order(self, price, volume, ask_bid):
        if ask_bid == 1:
            if price in self.bid:
                self.bid[price] += volume
            else:
                self.bid[price] = volume
        else:
            if price in self.ask:
                self.ask[price] += volume
            else:
                self.ask[price] = volume