import pickle
import numpy as np



def create_price_dictionary(file_2_name):
    # bid positive ask  negative
    with open(file_2_name, 'rb') as f:
        y_new_snp = pickle.load(f)

    price_dictionary = {}
    price_key =  range(y_new_snp[0].bid_price[30],y_new_snp[0].ask_price[30],1)

    for i in price_key:
        price_dictionary[i] = list([])

    for i in y_new_snp:
        bid_price = list(i.bid_price)
        ask_price = list(i.ask_price)
        for k in price_key:
            find = True
            for z, value in enumerate(bid_price):
                if value == k:
                    price_dictionary[k].append(i.bid_quantity[z])
                    find = False
            if find:
                for z, value in enumerate(ask_price):
                    if value == k:
                        price_dictionary[k].append( -i.ask_quantity[z])
                        find = False
            if find:
                price_dictionary[k].append(0)


    Duration =len(y_new_snp)
    y_mid_price = np.zeros([1,Duration])
    y_time = np.zeros([1,Duration])
    for i in range(0, Duration):
        y_mid_price[0,i] = y_new_snp[i].mid_price
        y_time[0,i]= y_new_snp[i].time

    return price_key, price_dictionary, y_mid_price, y_time


def create_y_list(price_key, price_dictionary):
    y_list = []
    for i in range(len(price_dictionary[price_key[0]])):
        temp = []
        for k in price_key:
            temp.append(price_dictionary[k][i])
        y_list.append(temp)
    return np.transpose(np.array(y_list))

def create_A(price_key, mid_price, C):
    n = len(price_key)
    a11 = np.eye(n)
    a22 = np.eye(2)
    a21 = np.zeros([2,n])
    a12 = np.array([price_key,price_key]) - mid_price
    a12 = pow(a12,2) +1
    a12 = np.transpose(C*1.0/a12)
    A = np.hstack((a11,a12))
    B = np.hstack((a21,a22))
    return np.vstack((A,B))

def create_A_2(price_key, mid_price, C):
    n = len(price_key)
    a11 = np.eye(n)
    a22 = np.eye(2)
    a21 = np.zeros([2,n])
    a12 = np.array([price_key,price_key]) - mid_price
    a12 = pow(a12,2) +1
    a12 = np.transpose(C*1.0/a12)
    for i in range(len(price_key)):
        if price_key[i]<mid_price:
            a12[i,1] = 0.0
        else:
            a12[i,0] = 0.0

    A = np.hstack((a11,a12))
    B = np.hstack((a21,a22))
    return np.vstack((A,B))








