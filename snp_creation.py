import datetime
import time
import copy
import pickle
from snp import snp
from collections import OrderedDict


def build_data(hour, file_id):
    order_dict = {}
    snp_dict = OrderedDict()
    print(file_id)
    file_name = '{:02d}{:02d}.tks'.format(hour,file_id)
    file_path = 'TKS_09_02/' + file_name
    file = open(file_path, 'r')
    time.clock()
    for line in file:
        line = line[:-1]
        temp_split = line.split(',')
        timestamp = temp_split[0]
        time_seconds = timestamp.split('.')
        x = time.strptime(time_seconds[0], '%H:%M:%S')
        time_convert = datetime.timedelta(
            hours=x.tm_hour, minutes=x.tm_min, seconds=x.tm_sec).total_seconds()
        time_convert = int(time_convert)*1000 + int(time_seconds[1])
        if temp_split[2] == 'S':
            identifier = int(temp_split[1])
            ask_bid = int(temp_split[3])
            price = int(float(temp_split[4])*1e5)
            volume = int(temp_split[5])
            if time_convert not in snp_dict:
                snp_dict[time_convert] = snp()
                snp_dict[time_convert].insert(price, volume, ask_bid)
            else:
                snp_dict[time_convert].insert(price, volume, ask_bid)
            order_dict[identifier] = [price, volume, ask_bid]
        elif temp_split[2] == 'N':
            identifier = int(temp_split[1])
            ask_bid = int(temp_split[3])
            price = int(float(temp_split[4])*1e5)
            volume = int(temp_split[5])
            if time_convert != previous_time:
                snp_dict[time_convert] = copy.deepcopy(snp_dict[previous_time])

            snp_dict[time_convert].new_order(price, volume, ask_bid)
            order_dict[identifier] = [price, volume, ask_bid]
        elif temp_split[2] == 'C':
            identifier = int(temp_split[1])
            ask_bid = order_dict[identifier][2]
            price = order_dict[identifier][0]
            volume = order_dict[identifier][1]
            if time_convert != previous_time:
                snp_dict[time_convert] = copy.deepcopy(snp_dict[previous_time])
            snp_dict[time_convert].cancel_order(price, volume, ask_bid)
            del order_dict[identifier]
        else:
            identifier = int(temp_split[1])
            ask_bid = order_dict[identifier][2]
            price = order_dict[identifier][0]
            volume = int(temp_split[5])
            if time_convert != previous_time:
                snp_dict[time_convert] = copy.deepcopy(snp_dict[previous_time])

            snp_dict[time_convert].cancel_order(price, order_dict[identifier][1],
                                                ask_bid)
            snp_dict[time_convert].new_order(price, volume, ask_bid)
            order_dict[identifier] = [price, volume, ask_bid]
        previous_time = time_convert



    print('parse finish')
    with open('snp_object/snp_{:02d}{:02d}.dat'.format(hour,file_id), 'wb') as f:
        pickle.dump(snp_dict, f, protocol=2)



hour = 2
file_id = 0
build_data(hour, file_id)