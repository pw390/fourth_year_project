from utils import create_price_dictionary, create_y_list
import numpy as np
import matplotlib.pyplot as pt
file_2_name = 'snp_parsed/snp_0200.dat'



price_key, price_dictionary, y_mid_price, y_time = create_price_dictionary(file_2_name)
y_list_save = create_y_list(price_key, price_dictionary)
y_list = y_list_save
#distance to mid_price vs change

change = [[0]]
Duration = y_time.shape[1]
c_table = np.zeros([len(price_key),1])
for i in range(1, Duration):
    dist = np.array([price_key]).T - y_mid_price[0,i-1]
    dist = pow(dist,2) +1
    y_1 = y_list[:, i].reshape((y_list[:, i].shape[0], 1))
    y_2 = y_list[:, i-1].reshape((y_list[:, i-1].shape[0], 1))
    h = np.multiply(abs(y_1-y_2),dist)
    change.append(np.std(h,axis = 0).tolist())
    c_table = np.hstack((c_table, h))


fig1 = pt.figure()

# and the first axes using subplot populated with data
ax1 = fig1.add_subplot(111)
line1 = ax1.plot(np.array(change))
line2 = ax1.plot(y_mid_price[0,:-1].T)
pt.show()


fig2 = pt.figure()

# and the first axes using subplot populated with data
ax1 = fig2.add_subplot(111)
line2 = ax1.plot(y_mid_price[0,:-1].T)
pt.show()




# plot result
import seaborn as sns
sns.set()
x_tick = ['' for y in range(4000)]
x_tick[:: 500] =range(0,4000,500)
y_tick = ['' for y in price_key]
y_tick[::-10] = price_key[::-10]
data = c_table.tolist()
ax = sns.heatmap(data,xticklabels=x_tick,yticklabels=y_tick)
