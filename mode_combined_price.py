from utils import create_A,create_price_dictionary, create_y_list, create_A_2
from kalman_filter import kf_predict,kf_update
import numpy as np
import matplotlib.pyplot as pt
file_2_name = 'snp_parsed/snp_0200.dat'

price_key, price_dictionary, y_mid_price, y_time = create_price_dictionary(file_2_name)
y_list_save = create_y_list(price_key, price_dictionary)


coefficient = 10.0
change_variance = 1.0
minimum_order = 1e5
y_list = y_list_save*1.0/minimum_order

N = len(price_key)

C = np.std(abs(y_list), axis = 1)
Q = np.diag(np.hstack((pow(C,2)/1000 +1, np.array([change_variance,change_variance])))) # noise
H = np.hstack((np.eye(N), np.zeros([N,2])))
R = np.diag(pow(C,2)*100)
y_inital = y_list[:,0].reshape(( y_list[:,0].shape[0], 1))
X = np.vstack((y_inital, np.array([[0
],[0
]])))
Q_table =np.array(y_inital)
X_table =np.array([[0
],[0]])
P = Q
ll = 0
for i in range(1, len(price_dictionary[price_key[0]])):
    A = create_A_2(price_key,y_mid_price[0,i-1],coefficient)
    X, P = kf_predict(X, P, A, Q, 0, 0)
    y = y_list[:, i].reshape((y_list[:, i].shape[0], 1))
    X, P, LH= kf_update(X,P, y,H,R)
    Q_table=np.hstack((Q_table, X[:N,:]))
    X_table=np.hstack((X_table,X[N:,:]))
    if np.isnan(LH):
        break
    ll += LH

print ll


# plot result
import seaborn as sns
sns.set()
x_tick = ['' for y in range(4000)]
x_tick[:: 500] =range(0,4000,500)
y_tick = ['' for y in price_key]
y_tick[::-10] = price_key[::-10]
data = Q_table.tolist()
ax = sns.heatmap(data, cmap='PiYG',center=0,xticklabels=x_tick,yticklabels=y_tick)

fig1 = pt.figure()
# and the first axes using subplot populated with data
ax1 = fig1.add_subplot(111)
line1 = ax1.plot(np.transpose(y_time[0,:]), X_table[0,:].T,label='bid')
pt.ylabel("Volume")
pt.legend(loc=4, fontsize="small")
pt.show()

# now, the second axes that shares the x-axis with the ax1
fig2 = pt.figure()
ax2= fig2.add_subplot(111)
line3 = ax2.plot(np.transpose(y_time[0,:]), X_table[1,:].T,'r',label='ask')
pt.ylabel("Volume")
pt.legend(loc=4, fontsize="small")
pt.show()





fig1 = pt.figure()
i = 20
# and the first axes using subplot populated with data
ax1 = fig1.add_subplot(111)
line1 = ax1.plot(np.transpose(y_time[0,:]), Q_table[i,:].T,label='filtered')
line2 = ax1.plot(np.transpose(y_time[0,:]), y_list[i,:].T,label='original')
pt.ylabel("Volume")
pt.legend(loc=2, fontsize="small")
pt.show()