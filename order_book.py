import numpy as np
class OrderBook():
    def __init__(self, time, bid_price, bid_quantity, ask_price, ask_quantity):
        self.time = time
        self.bid_price = np.flipud(bid_price)
        self.ask_price = ask_price
        self.bid_quantity = np.flipud(bid_quantity)
        self.ask_quantity = ask_quantity
        self._mid_price()
        self.min_size = None
        self.abs = False

    def  _mid_price(self):
        if len(self.ask_price)==0 and len(self.bid_price)==0:
            self.mid_price = 0
        elif len(self.ask_price)==0:
            self.mid_price = max(self.bid_price)
        elif len(self.bid_price)==0:
            self.mid_price = min(self.ask_price)
        else:
            self.mid_price = (max(self.bid_price) + min(self.ask_price)) / 2

    def miniportioned(self, mini_order_size):
        if self.min_size:
            self.recover()
        self.min_size = mini_order_size
        self.bid_quantity = self.bid_quantity / self.min_size
        self.ask_quantity = self.ask_quantity / self.min_size

    def recover(self):
        if self.min_size:
            self.bid_quantity = self.bid_quantity * self.min_size
            self.ask_quantity = self.ask_quantity * self.min_size

    def convert2absdifference(self):
        if not self.abs:
            self.bid_price = np.absolute(self.bid_price - self.mid_price)
            self.ask_price = np.absolute(self.ask_price - self.mid_price)

    def convert2origdifference(self):
        if self.abs:
            self.bid_price = self.mid_price - self.bid_price
            self.ask_price = self.mid_price - self.ask_price