import numpy as np
import pickle
import models as md
import plot as pl
import levy

file_name = 'SNP_2.dat'
with open(file_name,'rb') as f:
    y_t= pickle.load(f)


N = 5
# start time
start_time = 700
# end time
end_time = 710
# time spread
Duration = end_time - start_time + 1
# constant tick size
tick_size = 1e-5


# mini size
mini_size = 5e4

shape_parameter = 1.5
scale_parameter = 5e-5
shift_parameter = y_t[start_time].mid_price
skewness_parameter= 0.7
# variance setting
sigma_shape = 0.1
sigma_skewness = 0.1
sigma_scale = 3e-5
sigma_shift = 1e-5

# get absolute price difference
for i in y_t:
    i.miniportioned(mini_size)

# fit levy for the initial value
from collections import Counter

c = Counter()

for x, y in zip(y_t[0].bid_price, y_t[0].bid_quantity):
    c[x] = int(y)
est_bid_parameter = levy.fit_levy(list(c.elements()))
print(est_bid_parameter)

c.clear()

for x, y in zip(y_t[0].ask_price, y_t[0].ask_quantity):
    c[x] = int(y)
est_ask_parameter = levy.fit_levy(list(c.elements()))
print(est_ask_parameter)

model = md.AlphaStableModel(sigma_shape, sigma_skewness, sigma_scale,
                              sigma_shift)

bid_parameter = np.vstack((np.ones([1,N])*est_bid_parameter[0],
                           np.ones([1,N])*est_bid_parameter[1],
                           np.ones([1,N])*est_bid_parameter[3],
                           np.ones([1,N])*est_bid_parameter[2]))

ask_parameter = np.vstack((np.ones([1,N])*est_ask_parameter[0],
                           np.ones([1,N])*est_ask_parameter[1],
                           np.ones([1,N])*est_ask_parameter[3],
                           np.ones([1,N])*est_ask_parameter[2]))



# start bid part
ave_bid_parameter = np.zeros([4,Duration])
std_bid_parameter = np.zeros([4,Duration])

for i in range(start_time, end_time+1):
    print(i)
    bid_parameter= model.bid_sample(bid_parameter)
    weight = model.log_likelihood(y_t[i].bid_price, y_t[i].bid_quantity,
                                  bid_parameter, tick_size)
    weight = model.weight_normalisation(weight)
    bid_parameter = model.stratified_sampling(weight,bid_parameter)
    ave_bid_parameter[:,i-start_time] = np.mean(bid_parameter,axis = 1)
    std_bid_parameter[:,i-start_time]  = np.std(bid_parameter, axis=1)

ave_ask_parameter = np.zeros([4,Duration])
std_ask_parameter = np.zeros([4,Duration])
for i in range(start_time, end_time+1):
    print(i)
    ask_parameter= model.ask_sample(ask_parameter)
    weight = model.log_likelihood(y_t[i].bid_price, y_t[i].bid_quantity,
                                  ask_parameter, tick_size)
    weight = model.weight_normalisation(weight)
    ask_parameter = model.stratified_sampling(weight,ask_parameter)
    ave_ask_parameter[:,i-start_time] = np.mean(ask_parameter,axis = 1)
    std_ask_parameter[:,i-start_time]  = np.std(ask_parameter, axis=1)

pl.parameter_curve(ave_bid_parameter,std_bid_parameter)