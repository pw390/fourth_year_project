import pickle
import matplotlib.pyplot as pt
import numpy as np
file_2_name = 'snp_parsed/snp_0200.dat'

with open(file_2_name, 'rb') as f:
    y_t_new_snp = pickle.load(f)
Duration =len(y_t_new_snp)
y_mid_price = np.zeros([1,Duration])
y_time = np.zeros([1,Duration])
for i in range(0, Duration):
    y_mid_price[0,i] = y_t_new_snp[i].mid_price
    y_time[0,i]= y_t_new_snp[i].time


pt.plot(np.transpose(y_time),np.transpose(y_mid_price))


pt.show()