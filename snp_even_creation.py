import pickle
import numpy as np
import order_book as od

file_id = 0
hour =2
with open('snp_object/snp_{:02d}{:02d}.dat'.format(hour,file_id), 'rb') as handle:
     snp_dict= pickle.load(handle)

y_t = []

key_list = list(snp_dict.keys())
time_initial = key_list[0]
dict_pt = 0
time_interval = 15


for i in range(0, 60*1000/15):
    while dict_pt<len(key_list) and (time_initial + i * 15) >= key_list[dict_pt]:
        dict_pt += 1
    print(time_initial+i*15, key_list[dict_pt-1])
    t= key_list[dict_pt-1]
    bid_price = []
    bid_quantity = []
    ask_price = []
    ask_quantity = []

    for j in sorted(snp_dict[t].bid.keys()):
        bid_price.append(j)
        bid_quantity.append(snp_dict[t].bid[j])

    for j in sorted(snp_dict[t].ask.keys()):
        ask_price.append(j)
        ask_quantity.append(snp_dict[t].ask[j])

    bid_price = np.transpose(np.array(bid_price))
    bid_quantity = np.transpose(np.array(bid_quantity))
    ask_price = np.transpose(np.array(ask_price))
    ask_quantity = np.transpose(np.array(ask_quantity))
    y_t.append(
        od.OrderBook(i*15*1.0/1000+file_id*60+hour*3600+61200, bid_price, bid_quantity, ask_price, ask_quantity))
print('parse finish')
with open('snp_parsed/snp_{:02d}{:02d}.dat'.format(hour,file_id), 'wb') as f:
    pickle.dump(y_t, f, protocol=2)




