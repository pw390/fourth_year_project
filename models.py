import numpy as np
from scipy.stats import gamma
from scipy.stats import invgamma
import levy


class Model(object):
    def __init__(self, sigma_shape, sigma_scale, sigma_shift):
        self.sigma_shape = sigma_shape
        self.sigma_scale = sigma_scale
        self.sigma_shift = sigma_shift

    def log_likelihood(self, pdist, snp_price, snp_quantity, parameter, tick_size):
        [m, n] = pdist.shape
        weight = np.zeros([1, n])
        for i in range(n):
            hist_count = np.zeros(m)
            for j in range(len(snp_price)):
                ind = np.floor((snp_price[j] + parameter[2, i]) / tick_size) + 1
                if ind <m and ind >= 0:
                    hist_count[int(ind)] += round(snp_quantity[j])
            s = np.sum(hist_count)
            weight[0, i] += self.log_factorial(np.sum(hist_count))

            for k in hist_count:
                weight[0, i] -= self.log_factorial(k)
            weight[0, i] += np.sum(np.dot(hist_count, np.log(pdist[:, i])))
        return weight

    def log_factorial(self, n):
        logf = 0
        for i in range(1, int(n) + 1):
            logf += np.log(i)
        return logf

    def weight_normalisation(self, weight):
        max_weight = np.max(weight)

        n_weight = np.exp(weight - max_weight)

        n_weight = n_weight / np.sum(n_weight)
        return n_weight

    def stratified_sampling(self, weight, parameter):
        [m,n]= weight.shape
        c = np.cumsum(weight)
        new_parameter = np.zeros(parameter.shape)
        for i in range(int(n)):
            u = (np.random.rand() + i) / n
            first = np.where(c >= u)[0][0]
            new_parameter[:, i] = parameter[:, first]
        return new_parameter


class GammaModel(Model):
    def sample(self, original_parameter, support_num, tick_size, minimum):
        [m, n] = original_parameter.shape
        new_parameter = np.zeros([m, n])
        pdist = np.zeros([support_num, n])
        for i in range(n):
            temp_probability = np.zeros(support_num)
            while not (temp_probability > 0).all():
                # sample alpha
                new_parameter[0, i] = np.random.gamma( \
                    original_parameter[0, i] ** 2 / self.sigma_shape ** 2, \
                    self.sigma_shape ** 2 / original_parameter[0, i])
                while new_parameter[0][i] <= 1.3:
                    new_parameter[0, i] = np.random.gamma( \
                        original_parameter[0, i] ** 2 / self.sigma_shape ** 2, \
                        self.sigma_shape ** 2 / original_parameter[0, i])
                # sample beta
                new_parameter[1, i] = np.random.gamma( \
                    original_parameter[1, i] ** 2 / self.sigma_scale ** 2, \
                    self.sigma_scale ** 2 / original_parameter[1, i])
                new_parameter[2, i] = original_parameter[2, i] + self.sigma_shift * np.random.standard_normal()
                while new_parameter[2,i] > 0 or new_parameter[2,i]+minimum<=0:
                    new_parameter[2, i] = original_parameter[2, i] + self.sigma_shift * np.random.standard_normal()
                x = np.linspace(1 * tick_size, support_num * tick_size, support_num)
                temp_probability = np.copy(gamma.cdf(x, new_parameter[0, i], 0, new_parameter[1, i]))
                dtemp = np.hstack((np.arange(1), temp_probability[0:-1]))
                temp_probability = temp_probability - dtemp
                temp_probability = temp_probability / np.sum(temp_probability)

            pdist[:, i] = np.copy(temp_probability)
        return new_parameter, pdist
    def pdf(self,parameter,support_num, tick_size):
        temp_probability = np.zeros(support_num)
        x = np.linspace(1 * tick_size, support_num * tick_size, support_num)
        temp_probability = np.copy(gamma.cdf(x, parameter[0], 0, parameter[1]))
        dtemp = np.hstack((np.arange(1), temp_probability[0:-1]))
        temp_probability = temp_probability - dtemp
        temp_probability = temp_probability / np.sum(temp_probability)
        return temp_probability


class InverseGammaModel(Model):
    def sample(self, original_parameter, support_num, tick_size, minimum):
        [m, n] = original_parameter.shape
        new_parameter = np.zeros([m, n])
        pdist = np.zeros([support_num, n])
        for i in range(n):
            temp_probability = np.zeros(support_num)
            while not (temp_probability > 0).all():
                # sample alpha
                new_parameter[0, i] = np.random.gamma( \
                    original_parameter[0, i] ** 2 / self.sigma_shape ** 2, \
                    self.sigma_shape ** 2 / original_parameter[0, i])
                while new_parameter[0][i] <= 1.3:
                    new_parameter[0, i] = np.random.gamma( \
                        original_parameter[0, i] ** 2 / self.sigma_shape ** 2, \
                        self.sigma_shape ** 2 / original_parameter[0, i])
                # sample beta
                new_parameter[1, i] = np.random.gamma( \
                    original_parameter[1, i] ** 2 / self.sigma_scale ** 2, \
                    self.sigma_scale ** 2 / original_parameter[1, i])
                new_parameter[2, i] = original_parameter[2, i] + self.sigma_shift * np.random.standard_normal()
                while new_parameter[2, i] > 0 or new_parameter[2, i] + minimum <= 0:
                    new_parameter[2, i] = original_parameter[2, i] + self.sigma_shift * np.random.standard_normal()
                x = np.linspace(1 * tick_size, support_num * tick_size, support_num)
                temp_probability = np.copy(invgamma.cdf(x, new_parameter[0, i], 0, new_parameter[1, i]))
                dtemp = np.hstack((np.arange(1), temp_probability[0:-1]))
                temp_probability = temp_probability - dtemp
                temp_probability = temp_probability / np.sum(temp_probability)

            pdist[:, i] = np.copy(temp_probability)
        return new_parameter, pdist

    def pdf(self,parameter,support_num, tick_size):
        temp_probability = np.zeros(support_num)
        x = np.linspace(1 * tick_size, support_num * tick_size, support_num)
        temp_probability = np.copy(invgamma.cdf(x, parameter[0], 0, parameter[1]))
        dtemp = np.hstack((np.arange(1), temp_probability[0:-1]))
        temp_probability = temp_probability - dtemp
        temp_probability = temp_probability / np.sum(temp_probability)
        return temp_probability


class AlphaStableModel(Model):
    # shape stands for stability paramter
    def __init__(self, sigma_shape, sigma_skewness, sigma_scale, sigma_shift):
        super(AlphaStableModel, self).__init__(sigma_shape, sigma_scale,
                                                 sigma_shift)
        self.sigma_skewness = sigma_skewness

    def bid_sample(self, original_parameter):
        [m, n] = original_parameter.shape
        new_parameter = np.zeros([m, n])
        for i in range(n):
            # sample alpha
            new_parameter[0, i] = np.random.gamma( \
                original_parameter[0, i] ** 2 / self.sigma_shape ** 2, \
                self.sigma_shape ** 2 / original_parameter[0, i])
            while new_parameter[0][i] > 1:
                new_parameter[0, i] = np.random.gamma( \
                    original_parameter[0, i] ** 2 / self.sigma_shape ** 2, \
                    self.sigma_shape ** 2 / original_parameter[0, i])
            # sample beta
            new_parameter[1, i] = -np.random.gamma( \
                original_parameter[1, i] ** 2 / self.sigma_skewness ** 2, \
                self.sigma_skewness ** 2 / -original_parameter[1, i])

            new_parameter[2, i] = np.random.gamma( \
                original_parameter[2, i] ** 2 / self.sigma_scale ** 2, \
                self.sigma_scale ** 2 / original_parameter[2, i])
            new_parameter[3, i] = original_parameter[
                                      3, i] + self.sigma_shift * np.random.standard_normal()
            while new_parameter[3, i]< 0:
                new_parameter[3, i] = original_parameter[
                                          3, i] + self.sigma_shift * np.random.standard_normal()
        return new_parameter


    def ask_sample(self, original_parameter):
        [m, n] = original_parameter.shape
        new_parameter = np.zeros([m, n])
        for i in range(n):
            # sample alpha
            new_parameter[0, i] = np.random.gamma( \
                original_parameter[0, i] ** 2 / self.sigma_shape ** 2, \
                self.sigma_shape ** 2 / original_parameter[0, i])
            while new_parameter[0][i] > 1:
                new_parameter[0, i] = np.random.gamma( \
                    original_parameter[0, i] ** 2 / self.sigma_shape ** 2, \
                    self.sigma_shape ** 2 / original_parameter[0, i])
            # sample beta
            new_parameter[1, i] = np.random.gamma( \
                original_parameter[1, i] ** 2 / self.sigma_skewness ** 2, \
                self.sigma_skewness ** 2 / original_parameter[1, i])

            new_parameter[2, i] = np.random.gamma( \
                original_parameter[2, i] ** 2 / self.sigma_scale ** 2, \
                self.sigma_scale ** 2 / original_parameter[2, i])
            new_parameter[3, i] = original_parameter[
                                      3, i] + self.sigma_shift * np.random.standard_normal()
            while new_parameter[3, i]< 0:
                new_parameter[3, i] = original_parameter[
                                          3, i] + self.sigma_shift * np.random.standard_normal()
        return new_parameter

    def log_likelihood(self,snp_price, snp_quantity, parameter, tick_size):
        [m, n] = parameter.shape
        weight = np.zeros([1, n])
        for i in range(n):
            cdf_1 = levy.levy((snp_price+0.5*tick_size), parameter[0,i],
                                    parameter[1,i], parameter[3,i],
                                    parameter[2,i], True
                                    )
            cdf_2 = levy.levy((snp_price-0.5*tick_size), parameter[0,i],
                                    parameter[1,i], parameter[3,i],
                                    parameter[2,i], True
                                    )
            cdf_final = cdf_1 - cdf_2

            weight[0,i] = np.dot(snp_quantity,np.log(cdf_final))

        return weight


class GammaFlexMidpriceModel(Model):
    def __init__(self,sigma_alpha, sigma_beta, sigma_mid_price):
        self.sigma_alpha = sigma_alpha
        self.sigma_beta = sigma_beta
        self.sigma_mid_price = sigma_mid_price


    def sample(self, original_parameter):
        [m, n] = original_parameter.shape
        new_parameter = np.zeros([m, n])

        for i in range(n):
            # bid side
            new_parameter[0, i] = np.random.gamma( \
                original_parameter[0, i] ** 2 / self.sigma_alpha** 2, \
                self.sigma_alpha ** 2 / original_parameter[0, i])
            while new_parameter[0][i] <1:
                new_parameter[0, i] = np.random.gamma( \
                    original_parameter[0, i] ** 2 / self.sigma_alpha ** 2, \
                    self.sigma_alpha ** 2 / original_parameter[0, i])

            # sample beta
            new_parameter[1, i] = np.random.gamma( \
                original_parameter[1, i] ** 2 / self.sigma_beta ** 2, \
                self.sigma_beta ** 2 / original_parameter[1, i])


            #ask side
            new_parameter[2, i] = np.random.gamma( \
                original_parameter[2, i] ** 2 / self.sigma_alpha ** 2, \
                self.sigma_alpha ** 2 / original_parameter[2, i])
            while new_parameter[2][i] <1:
                new_parameter[2, i] = np.random.gamma( \
                    original_parameter[2, i] ** 2 / self.sigma_alpha ** 2, \
                    self.sigma_alpha ** 2 / original_parameter[2, i])

            # sample beta
            new_parameter[3, i] = np.random.gamma( \
                original_parameter[3, i] ** 2 / self.sigma_beta ** 2, \
                self.sigma_beta ** 2 / original_parameter[3, i])


            # mid_price
            new_parameter[4, i] = original_parameter[4, i] \
                            + self.sigma_mid_price * np.random.standard_normal()
        return new_parameter

    def log_likelihood(self, y, parameter, tick_size):

        return self.log_likelihood_one_side(y.bid_price, y.bid_quantity,
                                            parameter[0,:], parameter[1,:],
                                            parameter[4,:], tick_size, True) + \
               self.log_likelihood_one_side(y.ask_price, y.ask_quantity,
                                            parameter[2, :], parameter[3, :],
                                            parameter[4, :], tick_size, False)


    def log_likelihood_one_side(self, snp_price, snp_quantity,
                                alpha, beta, mid_price, tick_size, bid_or_ask):
        n = len(mid_price)
        weight = np.zeros([1,n])
        for i in range(n):
            if bid_or_ask:
                snp_price_copy = np.array([j for (k,j) in enumerate(snp_price)
                                    if j<=mid_price[i]])
                snp_price_copy= -snp_price_copy +mid_price[i]
                snp_quantity_copy = np.array([snp_quantity[k]
                                     for (k,j) in enumerate(snp_price)
                                     if j<=mid_price[i]])
            else:
                snp_price_copy = np.array([j for (k,j) in enumerate(snp_price)
                                    if j>=mid_price[i]])
                snp_price_copy= snp_price_copy-mid_price[i]
                snp_quantity_copy = np.array([snp_quantity[k]
                                     for (k,j) in enumerate(snp_price)
                                     if j>=mid_price[i]])
            cdf_1= np.copy(
                gamma.cdf(snp_price_copy + 0.5*tick_size, alpha[i],
                          0, beta[i]))
            snp_price_copy = snp_price_copy - 0.5 * tick_size
            for k in range(len(snp_price_copy)):
                if snp_price_copy[k]<0:
                    snp_price_copy[k]=0
            cdf_2= np.copy(
                gamma.cdf( snp_price_copy, alpha[i],
                          0, beta[i]))
            cdf = cdf_1 - cdf_2
            # tick the last item out as precision not allowed
            snp_quantity_copy = np.array([snp_quantity_copy[k]
                                          for (k, j) in enumerate(cdf)
                                          if j >0])
            cdf = np.array([j for (k, j) in enumerate(cdf)
                                       if j > 0])

            weight[0,i] = np.dot(snp_quantity_copy,np.log(cdf))

        return weight


