import numpy as np
import matplotlib as mt
import pickle
import order_book as od
import models as md
import plot as pl


file_name = 'SNP_2.dat'
with open(file_name,'rb') as f:
    y_t= pickle.load(f)

N = 500
# start time
start_time = 700
# end time
end_time = 710
# time spread
Duration = end_time - start_time + 1
# constant tick size
tick_size = 1e-5
# number of bins of multinomial distribution
support_num = 40
# model choose Ga for True IG for False
model_choice = True

# mini size
mini_size = 5e4

if model_choice:
    # Gamma model
    # initial parameter
    shape_parameter = 1.5
    scale_parameter = 5e-5
    shift_parameter = 0
    # variance setting
    sigma_shape = 0.1
    sigma_scale = 3e-5
    sigma_shift = 1e-5
else:
    # IG distribution
    # initial parameter
    shape_parameter = 1.5
    scale_parameter = 5e-5
    shift_parameter = 0
    # variance setting
    sigma_shape = 0.1
    sigma_scale = 3e-5
    sigma_shift = 1e-5

#select model
if model_choice:
    model = md.GammaModel(sigma_shape,sigma_scale,sigma_shift)
else:
    model = md.InverseGammaModel(sigma_shape,sigma_scale,sigma_shift)

bid_parameter = np.vstack((np.ones([1,N])*shape_parameter,np.ones([1,N])*scale_parameter,\
                         np.ones([1,N])*shift_parameter))
ask_parameter = np.vstack((np.ones([1,N])*shape_parameter,np.ones([1,N])*scale_parameter,\
                         np.ones([1,N])*shift_parameter))


#get absolute price difference
for i in y_t:
    i.convert2absdifference()
    i.miniportioned(mini_size)
# start bid part
ave_bid_parameter = np.zeros([3,Duration])
std_bid_parameter = np.zeros([3,Duration])

for i in range(start_time, end_time+1):
    print(i)
    bid_parameter, pdist = model.sample(bid_parameter,support_num,tick_size,y_t[i].bid_price[0])
    weight = model.log_likelihood(pdist,y_t[i].bid_price,y_t[i].bid_quantity, bid_parameter, tick_size)
    weight = model.weight_normalisation(weight)
    bid_parameter = model.stratified_sampling(weight,bid_parameter)
    ave_bid_parameter[:,i-start_time] = np.mean(bid_parameter,axis = 1)
    std_bid_parameter[:,i-start_time]  = np.std(bid_parameter, axis=1)

pl.parameter_curve(ave_bid_parameter,std_bid_parameter)

