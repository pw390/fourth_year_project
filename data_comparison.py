import pickle
import matplotlib.pyplot as pt
import numpy as np

file_name = 'SNP_2.dat'

with open(file_name, 'rb') as f:
    y_t_old_snp = pickle.load(f)
Duration =len(y_t_old_snp)
y_mid_price = np.zeros([1,Duration])

for i in range(0, Duration):
    y_mid_price[0,i] = y_t_old_snp[i].mid_price
print (y_mid_price)
pt.plot(np.transpose(y_mid_price))


pt.show()
'''
import csv

y = y_t_old_snp[1]
with open('csv_files/old_bid.csv', 'w') as csvfile:
    fieldnames = ['bid_price', 'bid_quantity']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    for i,j in zip(y.bid_price, y.bid_quantity):
        writer.writerow({'bid_price': i, 'bid_quantity': j})

with open('csv_files/old_ask.csv', 'w') as csvfile:
    fieldnames = ['ask_price', 'ask_quantity']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    for i,j in zip(y.ask_price, y.ask_quantity):
        writer.writerow({'ask_price': i, 'ask_quantity': j })


y = y_t_new_snp[8]
with open('csv_files/new_bid.csv', 'w') as csvfile:
    fieldnames = ['bid_price', 'bid_quantity']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    for i,j in zip(y.bid_price, y.bid_quantity):
        writer.writerow({'bid_price': i, 'bid_quantity': j})

with open('csv_files/new_ask.csv', 'w') as csvfile:
    fieldnames = ['ask_price', 'ask_quantity']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    for i,j in zip(y.ask_price, y.ask_quantity):
        writer.writerow({'ask_price': i, 'ask_quantity': j })
'''