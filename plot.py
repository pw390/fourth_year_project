import matplotlib.pyplot as pl
import matplotlib.patches as pa
import matplotlib.colorbar as cb
import colorsys


def reconstruction(y_t, start_time, end_time,tick_size):
    fig5 = pl.figure()
    ax = fig5.add_subplot(111)
    for i in range(start_time,end_time+1):
        y_t[i].convert2origdifference()
        max_quantity= max(max(y_t[i].bid_quantity),max(y_t[i].ask_quantity))
        n= len(y_t[i].bid_price)
        for j in range(n):
            k=pa.Rectangle([i,y_t[i].bid_price[j]-tick_size/2],1,tick_size, \
                facecolor ='r' , edgecolor=None)
            ax.add_patch(k)
        for x,y in zip(y_t[i].ask_price, y_t[i].ask_quantity):
            k=pa.Rectangle([i,x-tick_size/2],1,tick_size, \
                facecolor =colorsys.hsv_to_rgb(2.0/3-2.0/3*y/max_quantity,1.0,1.0) , edgecolor=None)
            ax.add_patch(k)
    pl.show()


def heat_map(model,mid_price,bid_parameter, ask_parameter, support_num, tick_size):
    pl.figure()
    for i in range(len(mid_price)):
        bid_pdf = model.pdf(bid_parameter,support_num,tick_size)
        ask_pdf = model.pdf(ask_parameter,support_num,tick_size)
        maxpdf= max(max(bid_pdf),max(ask_pdf))
        for i in range(support_num):
            pa.Rectange([i,mid_price[i]-tick_size*(i+1)+bid_parameter[2,i]],1.8,tick_size, \
                facecolor =colorsys.hsv_to_rgb(2.0/3-2.0/3*bid_pdf[i]/maxpdf,1.0,1.0) , edgecolor=None)
        for i in range(support_num):
            pa.Rectange([i,mid_price[i]-tick_size*(i+1)+ask_parameter[2,i]],1.8,tick_size, \
                facecolor =colorsys.hsv_to_rgb(2.0/3-2.0/3*ask_pdf[i]/maxpdf,1.0,1.0) , edgecolor=None)
    cb.Colorbar()
    pl.show()


def parameter_curve(parameter,std, name_list):
    fig=pl.figure()
    fig.suptitle('Plots of Parameters', fontsize=20)
    [m,n] = parameter.shape
    x = range(n)
    for i in range(m):
        pl.subplot(m,1,i+1)
        p1=pl.errorbar(x,parameter[i,:],yerr = std[i,:],label=name_list[i], ecolor='g', capthick=2)
    pl.legend()
    pl.xlabel('time point', fontsize=18)
    pl.show()

