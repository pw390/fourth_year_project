import pandas as pd
import pickle
import order_book as od


file_name = 'SNP_2'
postfix = '.csv'
# load  csv file
snp_data = pd.read_csv('Hourly/'+file_name+postfix, header=None)
snp_data = pd.DataFrame(snp_data)

# sort out data
Time = pd.unique(snp_data[0])

snp_grouped= snp_data.groupby(0)
y_t=[]
t= 61200
j = 0
hour = 2
t= t+hour*3600
for i in Time:
    snp_time_group = snp_grouped.get_group(i)
    snp_op_group = snp_time_group.groupby(1)
    bid_price = snp_op_group.get_group(1)[2].as_matrix()
    bid_quantity = snp_op_group.get_group(1)[3].as_matrix()
    ask_price = snp_op_group.get_group(2)[2].as_matrix()
    ask_quantity = snp_op_group.get_group(2)[3].as_matrix()
    y_t.append(od.OrderBook(i,bid_price,bid_quantity,ask_price,ask_quantity))
    if (i - t)>(j+1)*60:
        object_postfix = '.dat'
        with open('snp_original/snp_{:02d}{:02d}.dat'.format(hour,j), 'wb') as f:
            pickle.dump(y_t[:-1], f, protocol=2)
        y_t = [y_t[-1]]
        j+=1
    with open('snp_original/snp_{:02d}{:02d}.dat'.format(hour,j), 'wb') as f:
        pickle.dump(y_t[:-1], f, protocol=2)

with open('snp_original/snp_{:02d}{:02d}.dat'.format(hour,j-1), 'wb') as f:
    pickle.dump(y_t, f, protocol=2)