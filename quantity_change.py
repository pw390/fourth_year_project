import pickle
import matplotlib.pyplot as pt
import numpy as np
file_2_name = 'snp_parsed/snp_0200.dat'
# bid positive ask  negative
with open(file_2_name, 'rb') as f:
    y_new_snp = pickle.load(f)


price_dictionary = {}
price_key =  range(y_new_snp[0].bid_price[30],y_new_snp[0].ask_price[30],1)

for i in price_key:
    price_dictionary[i] = list([])

for i in y_new_snp:
    bid_price = list(i.bid_price)
    ask_price = list(i.ask_price)
    for k in price_key:
        find = True
        for z, value in enumerate(bid_price):
            if value == k:
                price_dictionary[k].append(i.bid_quantity[z])
                find = False
        if find:
            for z, value in enumerate(ask_price):
                if value == k:
                    price_dictionary[k].append( -i.ask_quantity[z])
                    find = False
        if find:
            price_dictionary[k].append(0)

#for i in range(len(price_key),15):
#   pt.plot(range(len(y_new_snp)), price_dictionary[price_key[i]])
#  pt.show()



Duration =len(y_new_snp)
y_mid_price = np.zeros([1,Duration])
y_time = np.zeros([1,Duration])
for i in range(0, Duration):
    y_mid_price[0,i] = y_new_snp[i].mid_price
    y_time[0,i]= y_new_snp[i].time

#distance to mid_price vs change
key = price_key[30]
change = [0]

for i in range(1, Duration):
    change.append(price_dictionary[key][i]-price_dictionary[key][i-1])

fig1 = pt.figure()

# and the first axes using subplot populated with data
ax1 = fig1.add_subplot(111)
line1 = ax1.plot(np.transpose(y_time), np.transpose(1/pow(key-y_mid_price,2)))
pt.ylabel("price")

# now, the second axes that shares the x-axis with the ax1
ax2 = fig1.add_subplot(111, sharex=ax1, frameon=False)
line2 = ax2.plot(np.transpose(y_time), abs(np.array(change)),'r')
ax2.yaxis.tick_right()
ax2.yaxis.set_label_position("right")
pt.ylabel("volume")

# for the legend, remember that we used two different axes so, we need
# to build the legend manually
pt.legend((line1, line2), ("mid_price", "change"))
pt.show()


data = []

for key in price_key[::-1]:
    data.append(np.array(price_dictionary[key]))


import seaborn as sns
sns.set()
x_tick = ['' for y in range(Duration)]
x_tick[:: 500] =range(0,4000,500)
y_tick = ['' for y in price_key]
y_tick[::-10] = price_key[::-10]
ax = sns.heatmap(data, cmap= 'PiYG',center=0,xticklabels=x_tick,yticklabels=y_tick)

## kalman filter
from kalman_filter import kf_predict,kf_update
key = price_key[10]
volume_list = price_dictionary[key]

Q_table =[]
X_table = []

C = np.std(abs(np.array(volume_list)))
Q = np.array([[pow(C,2),0],[0,pow(C,2)]])
H = np.array([[1,0]])
R = np.array([[pow(C,2)]])
X = np.array([[volume_list[0]],[volume_list[0]*1.0/100]])
P = 0
ll = 0
for i in range(1,len(volume_list)):
    A = np.array([[1,100.0/(1+pow(key-y_mid_price[0,i-1] ,2))],[0,1]])
    X, P = kf_predict(X, P, A, Q, 0, 0)
    Q_table.append(list(X[0])[0])
    X_table.append(list(X[1])[0])
    X, P, LH= kf_update(X,P,np.array([[volume_list[i]]]),H,R)
    ll += LH


print ll




fig1 = pt.figure()

# and the first axes using subplot populated with data
ax1 = fig1.add_subplot(111)
line1 = ax1.plot(np.transpose(y_time[0,1:]), np.array(volume_list[1:]),label='original')
line2 = ax1.plot(np.transpose(y_time[0,1:]), np.array(Q_table),label= 'filtered')
pt.ylabel("Volume")
pt.legend(loc=2, fontsize="small")

# now, the second axes that shares the x-axis with the ax1
ax2 = fig1.add_subplot(111, sharex=ax1, frameon=False)
line3 = ax2.plot(np.transpose(y_time[0,1:]), np.array(X_table),'r',label='trend')
ax2.yaxis.tick_right()
ax2.yaxis.set_label_position("right")
pt.ylabel("volume")

# for the legend, remember that we used two different axes so, we need
# to build the legend manually
pt.legend(loc=4, fontsize="small")
pt.show()


def filtered_Q(volume_list):
    Q_table = []
    X_table = []

    C = np.std(abs(np.array(volume_list)))
    Q = np.array([[pow(C, 2) / 1000, 0], [0, pow(C, 2) / 1000000]])
    H = np.array([[1, 0]])
    R = np.array([[pow(C, 2)*100]])
    X = np.array([[volume_list[0]], [volume_list[0] * 1.0 / 100]])
    P = 0
    for i in range(1, len(volume_list)):
        A = np.array(
            [[1, 100.0 / (1 + pow(key - y_mid_price[0, i - 1], 2))], [0, 1]])
        X, P = kf_predict(X, P, A, Q, 0, 0)
        Q_table.append(list(X[0])[0])
        X_table.append(list(X[1])[0])
        X, P = kf_update(X, P, volume_list[i], H, R)


    return Q_table, X_table
Q_map = []
X_map = []

for key in price_key[::-1]:
    Q_table, X_table = filtered_Q(price_dictionary[key])
    Q_map.append(Q_table)
    X_map.append(X_table)

sns.set()
x_tick = ['' for y in range(Duration)]
x_tick[:: 500] =range(0,4000,500)
y_tick = ['' for y in price_key]
y_tick[::-10] = price_key[::-10]
ax = sns.heatmap(Q_map, cmap= 'PiYG',center=0,xticklabels=x_tick,yticklabels=y_tick)




